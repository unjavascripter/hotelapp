"use strict";

(function(){

  function ctrlFn(dataFactory) {
    this.$onInit = _ => {
      let promise = dataFactory.get();
      promise.then(data => {this.hoteles = data.data});
    }
    this.alertIt = function(m) {
      console.log(m);
    }
  }
  ctrlFn.$inject = ['dataFactory'];

  angular
    .module('hotelApp')
      .component('main', {
        bindings: {
          name: '@'
        },
        transclude: true,
        controller: ctrlFn,
        template: `
          <div class="hotels-container>
            <div class="hotel card" ng-repeat="unHotel in $ctrl.hoteles">
              <hotel hotel-data="unHotel" goo="$ctrl.alertIt(message)"></hotel>
            </div>
          </div>
        `
       });
})();