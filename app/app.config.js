"use strict";

(function(){
  
  angular
    .module('hotelApp')
      .config(function ($urlRouterProvider, $compileProvider, $httpProvider) {
        // Default redirect
        $urlRouterProvider.otherwise('/');
        
        // Interceptor
        $httpProvider.interceptors.push('httpInterceptor');

        // Perf
        $compileProvider.debugInfoEnabled(false);
      });

})();