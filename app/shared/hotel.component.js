"use strict";

(function(){

  function ctrlFn() {
    console.log(this.hotelData);
    
    //this.hotelData = {name: "meee"};

    // this.$onChanges = (ov,nv) =>{
    //   console.log(ov);
    //   console.log(nv);
    // }
    this.obj = {"a": 4}
  }

  angular
    .module('hotelApp')
      .component('hotel', {
        bindings: {
          hotelData: '<',
          detailed: '@',
          goo: '&'
        },
        controller: ctrlFn,
        template: `
            
          <md-card>
            <md-card-title>
              <md-card-title-text>
                <a href="#/hotel/{{$ctrl.hotelData._id}}" class="md-headline" ng-if="!$ctrl.detailed">{{$ctrl.hotelData.name}}</a>
                <span href="#/hotel/{{$ctrl.hotelData._id}}" class="md-headline" ng-if="$ctrl.detailed">{{$ctrl.hotelData.name}}</span>
                <span class="md-subhead">{{$ctrl.hotelData.location.city}}, {{$ctrl.hotelData.location.country}}</span>
                <md-card-content>
                  <span ng-repeat="feature in $ctrl.hotelData.features">
                    {{feature | uppercase}}<span ng-if="!$last">, </span>
                  </span>
                  <p>
                    {{$ctrl.hotelData.description}}
                  </p>
                </md-card-content>
              </md-card-title-text>
              <div layout-padding>
                <div>Calificación: {{$ctrl.hotelData.rating}}/5</div>
                <div>Habitaciones: {{$ctrl.hotelData.totalRooms}}</div>
                <div>Reservaciones: {{$ctrl.hotelData.reservations}}</div>
              </div>
              <md-card-title-media>
                <div class="md-media-lg card-media">
                  <img ng-src="{{$ctrl.hotelData.pictures[0]}}" class="md-card-image">
                </div>
              </md-card-title-media>
            </md-card-title>
            
            <div ng-if="$ctrl.detailed">
              <div class="md-media-lg card-media" ng-repeat="picture in $ctrl.hotelData.pictures" layout-padding>
                <img ng-src="{{picture}}" class="md-card-image">
              </div>
              
              <div layout-padding layout="column" layout-align="center center">
                <a ng-href="https://www.google.com.co/maps/place/Hotel+{{$ctrl.hotelData.name}}/@{{$ctrl.hotelData.location.geo.lat}},{{$ctrl.hotelData.location.geo.lng}},17z" target="_blank">
                  <img src="http://maps.googleapis.com/maps/api/staticmap?center={{$ctrl.hotelData.location.geo.lat}},{{$ctrl.hotelData.location.geo.lng}}&size=500x500&zoom=14" style="width: auto;">
                </a>
              </div>
            </div>
            <md-card-actions layout="row" layout-align="end center">
              <md-button>Borrar</md-button>
              <md-button ng-click="$ctrl.goo({message: 1})">Editar</md-button>
            </md-card-actions>
          </md-card>

        `
       });
})();