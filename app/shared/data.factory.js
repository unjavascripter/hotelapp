"use strict";

(function(){
  function dataFactory(pesoshachetetepe) {
    let _dataFactory_ = {};

    _dataFactory_.get = _ => {
      return pesoshachetetepe.get('http://localhost:3000/api/hotels');
    }
    
    _dataFactory_.getOne = id => {
      return pesoshachetetepe.get(`http://localhost:3000/api/hotels/${id}`).then(response => response.data);
    }

    return _dataFactory_
  }

  dataFactory.$inject = ['$http'];

  angular
    .module('hotelApp')
      .factory('dataFactory', dataFactory);

})();