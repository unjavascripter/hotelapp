"use strict";

(function(){
  function config($stateProvider) {
    function ctrlFn (dataFactory, $stateParams, $state, $q) {

      if(!$stateParams.id){
        $state.go("main")
      }else{

        // With promise
        let promise = dataFactory.getOne($stateParams.id);
        promise
          .then(
            response => this.hotelData = response,
            err => console.log(err)
          )

        // Without promise
        //this.hotelData = promise;
      }

      
      let getTrueOrFalse = _ => {

        return $q((resolve, reject) => {
          setTimeout(function() {
            let value = Math.random() >= 0.5;

            if(value){
              resolve('is true!!!');
            }else{
              reject('its false :/');
            }
          }, 4000);
        }) 
        
      }

      let isTrue = getTrueOrFalse().then(response => {
        console.info(response);
        return response
      }, failure => {
        console.warn(failure);
      })
      
      .then(value => {
        if(value){
          console.info('😀');
        }else{
          console.warn('😣');
        }
      });

    }
  

    ctrlFn.$inject = ['dataFactory', '$stateParams', '$state', '$q'];

    $stateProvider
      .state('hotel', {
        "url": '/hotel/:id',
        "template": '<hotel detailed="true" hotel-data="$ctrl.hotelData"></hotel>',
        "controller": ctrlFn,
        "controllerAs": "$ctrl"
      })
  }
  config.$inject = ['$stateProvider'];

  angular
    .module('hotelApp')
      .config(config);

})();